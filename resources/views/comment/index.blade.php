@extends('layouts.master')
@section('title')
    Comments
@endsection
@section('content')
@forelse ($post as $key=>$value)
<div class="card mb-4">
  <div class="card-header p-2">
      <div class="post">
          <div class="user-block">
            <span class="username">
              <a href="#">{{$value->profile_id}}</a>
              <a href="#" class="float-right btn-tool"><i class="fa fa-times"></i></a>
            </span>
            <br>
            <span class="description">{{$value->created_at}}</span>
          </div>
      </div>
  </div>
  <div class="card-body">
    <div class="tab-content">
      <h4>{{$value->title}}</h4>
      <p>{{$value->body}}</p>
      <p>{{$value->caption}}</p>
    </div>
  </div>
  @empty
          <tr colspan="3">
              <td>You haven't create a comment</td><br><br>
          </tr> 
@endforelse
<hr>
@endsection