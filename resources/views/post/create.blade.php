@extends('layouts.master')
@section('title')
    Create Post
@endsection
@section('content')
{{-- CSS untuk image --}}
<style>
  .uploader {
  display: block;
  clear: both;
  margin: 0 auto;
  width: 100%;
  max-width: 600px;
  }
  .uploader label {
  float: left;
  clear: both;
  width: 100%;
  padding: 2rem 1.5rem;
  text-align: center;
  background: #fff;
  border-radius: 7px;
  border: 3px solid #eee;
  transition: all .2s ease;
  user-select: none;
  }
  .uploader label:hover {
  border-color: #454cad;
  }
  .uploader label.hover {
  border: 3px solid #454cad;
  box-shadow: inset 0 0 0 6px #eee;
  }
  .uploader label.hover #start i.fa {
  transform: scale(0.8);
  opacity: 0.3;
  }
  .uploader #start {
  float: left;
  clear: both;
  width: 100%;
  }
  .uploader #start.hidden {
  display: none;
  }
  .uploader #start i.fa {
  font-size: 50px;
  margin-bottom: 1rem;
  transition: all .2s ease-in-out;
  }
  .uploader #response {
  float: left;
  clear: both;
  width: 100%;
  }
  .uploader #response.hidden {
  display: none;
  }
  .uploader #response #messages {
  margin-bottom: .5rem;
  }
  .uploader #file-image {
  display: inline;
  margin: 0 auto .5rem auto;
  width: auto;
  height: auto;
  max-width: 180px;
  }
  .uploader #file-image.hidden {
  display: none;
  }
  .uploader #notimage {
  display: block;
  float: left;
  clear: both;
  width: 100%;
  }
  .uploader #notimage.hidden {
  display: none;
  }
  .uploader progress,
  .uploader .progress {
  display: inline;
  clear: both;
  margin: 0 auto;
  width: 100%;
  max-width: 180px;
  height: 8px;
  border: 0;
  border-radius: 4px;
  background-color: #eee;
  overflow: hidden;
  }
  .uploader .progress[value]::-webkit-progress-bar {
  border-radius: 4px;
  background-color: #eee;
  }
  .uploader .progress[value]::-webkit-progress-value {
  background: linear-gradient(to right, #393f90 0%, #454cad 50%);
  border-radius: 4px;
  }
  .uploader .progress[value]::-moz-progress-bar {
  background: linear-gradient(to right, #393f90 0%, #454cad 50%);
  border-radius: 4px;
  }
  .uploader input[type="file"] {
  display: none;
  }
  .uploader div {
  margin: 0 0 .5rem 0;
  color: #5f6982;
  }
  .uploader .btn {
  display: inline-block;
  margin: .5rem .5rem 1rem .5rem;
  clear: both;
  font-family: inherit;
  font-weight: 700;
  font-size: 14px;
  text-decoration: none;
  text-transform: initial;
  border: none;
  border-radius: .2rem;
  outline: none;
  padding: 0 1rem;
  height: 36px;
  line-height: 36px;
  color: #fff;
  transition: all 0.2s ease-in-out;
  box-sizing: border-box;
  background: #454cad;
  border-color: #454cad;
  cursor: pointer;
  }
  .text-danger{
    color: red;
  }
</style>
    <form role="form" action="/post" method="POST">
    @csrf
        <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Post Form</h3>
            </div>
              <div class="card-body">
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                </div>
                <div class="form-group">
                    <label for="body">Body</label>
                    <input type="text" class="form-control" id="body" name="body" placeholder="Body">
                </div>
                {{-- Membuat input gambar --}}
                <input id="images" type="file" name="images" accept="image/*" onchange="readURL(this);">
                <label for="images" id="file-drag">
                    <img id="file-image" src="#" alt="Preview" class="hidden">
                    <div id="start" >
                        <i class="fa fa-download" aria-hidden="true"></i>
                        <div>Select a file or drag here</div>
                        <div id="notimage" class="hidden">Please select an image</div>
                        <span id="file-upload-btn" class="btn btn-primary">Select a file</span>
                        <br>
                        <span class="text-danger">{{ $errors->first('images') }}</span>
                    </div>
                </label>
                {{-- End of Membuat input gambar --}}
                <div class="form-group">
                  <label for="caption">Caption</label>
                  <input type="text" class="form-control" id="caption" name="caption" placeholder="Caption for image posts">
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Post</button>
              </div>
        </div>
    </form>
{{-- Script untuk mengupload image --}}
    <script>
      function readURL(input, id) {
        id = id || '#file-image';
        if (input.files &amp;&amp; input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $(id).attr('src', e.target.result);
            };
    
            reader.readAsDataURL(input.files[0]);
            $('#file-image').removeClass('hidden');
            $('#start').hide();
        }
     }
    </script> 
@endsection