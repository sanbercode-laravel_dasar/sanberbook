@extends('layouts.master')
@section('title')
    Post {{$post->id}}
@endsection
@section('content')
<div class="card mb-4">
    <div class="card-header p-2">
        <div class="post">
            <div class="user-block">
              <span class="username">
                <a href="#">{{$post->profile->first_name}}</a>
                <a href="#" class="float-right btn-tool"><i class="fa fa-times"></i></a>
              </span>
              <br>
              <span class="description">{{$post->created_at}}</span>
            </div>
        </div>
    </div>
    <div class="card-body">
      <div class="tab-content">
        <h4>{{$post->title}}</h4>
        <p>{{$post->body}}</p>
        <p>{{$post->caption}}</p>
      </div>
    </div>
</div>

@foreach ($post->comments as $item)
  <div class="card mb-2">
      {{$item->body}}
  </div>
  @endforeach

<form role="form" action="{{route('comment.store',$post->id)}}" method="POST">
    @csrf
      <div class="card-body">
          <div class="form-group">
              <label for="comment">Comment</label>
              <input type="text" class="form-control" id="comment" name="body" placeholder="send a comment">
            </div>
            <button type="submit" class="btn btn-primary float-right">Send</button>
      </div>
</form>

@endsection