@extends('layouts.master')
@section('title')
    Home
@endsection
@section('content')
  @forelse ($posts as $key=>$value)
   {{--  Post --}}
    <div class="card mb-4">
      {{-- Post Header --}}
      <div class="card-header p-2">
          <div class="post">
              <div class="user-block">
                <span class="username">
                  <a href="#">{{$value->profile->first_name}}</a>
                  <a href="#" class="float-right btn-tool"><i class="fa fa-times"></i></a>
                </span>
                <br>
                <span class="description">{{$value->created_at}}</span>
              </div>
          </div>
      </div>
      {{-- Post Header Wrapper --}}
      {{-- Post Body --}}
      <div class="card-body">
        <div class="tab-content">
          <h4>{{$value->title}}</h4>
          <p>{{$value->body}}</p>
          <p>{{$value->caption}}</p>
          <p>
              <a href="#" class="link-black text-sm mr-3 like"><i class="fa fa-thumbs-up like" name ="reaction"></i> Like</a>
              <a href="#" class="link-black text-sm like"><i class="fa fa-thumbs-down dislike" name="reaction"></i> Dislike</a>
              <span class="float-right">
                <a href="post/{{$value->id}}" class="link-black text-sm">
                  <i class="fa fa-comments"></i> Comments (5)
                </a>
              </span>
          </p>
        </div>
      </div>
      {{-- Post Body wrapper --}}
    {{-- Comment --}}
    {{-- <div class="card-footer">
      <input class="form-control form-control-sm float-left col-sm-9" type="text" placeholder="Type a comment">
      <form action="/comment/{{$value->id}}" method="POST">
        @csrf
        <input type="submit" class="btn btn-primary col-sm-2 float-right" value="Send">
      </form>
    </div> --}}
    {{-- Comment Wrapper --}}
  </div>
  {{-- Post Wrapper --}}
      @empty
          <tr colspan="3">
              <td>You haven't create a post</td><br><br>
          </tr>  
  @endforelse
<hr>
<script>
  $('.like').on(click, function (event) { 
    // var isLike = event.target.previousElementSibling == null ?? true : false;
    console.log(event);
   });
  </script>
  
@endsection

  {{--    $('.dislike').click(function () { dislikeFunction(this);});
   });

   function likeFunction(caller) {
    
      var postId = <html>{{$value->id}}</html>;
      $.ajax({
          type: "POST",
          url: "post/index",
          data: 'Action=LIKE&PostID=' + postId,
          success: function () {}
      });
   }
   function dislikeFunction(caller) {
     var postId = caller.parentElement.getAttribute('postid');
     $.ajax({
         type: "POST",
         url: "",
         data: 'Action=DISLIKE&PostID=' + postId,
         success: function () {}
     });
   } --}}

 