@extends('layouts.master')
@section('title')
    My Profile
@endsection
@section('content')
<div class="card bg-light">
    <div class="card-header text-muted border-bottom-0">
        My Profile
    </div>
    <div class="card-body pt-0">
        <h2>First Name : {{Auth::user()->profile->first_name}}</h2>
        <h2>Last Name : {{Auth::user()->profile->last_name}}</h2>
        <h2>Date of Birth : {{Auth::user()->profile->date_of_birth}}</h2>
        <h2>Place of Birth : {{Auth::user()->profile->place_of_birth}}</h2>
    </div>
    <div class="card-footer">
        <a href="/profile/{{Auth::id()}}/edit" class="btn btn-primary float-left">Edit</a>
    </div>
</div>

@empty(Auth::user()->profile)
<tr colspan="3">
    <td>You haven't create a profile</td><br><br>
</tr>
@endempty
    
    
    
@endsection