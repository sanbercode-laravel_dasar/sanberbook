@extends('layouts.master')
@section('title')
    Profile
@endsection
@section('content')
    
    @forelse ($profile as $key=>$value)
                @if ( $value->id != Auth::id() )
                <div class="card bg-light mb-4">
                    <div class="card-header text-muted border-bottom-0">
                        Friend
                    </div>
                    <div class="card-body pt-0">
                        <h2>First Name : {{$value->first_name}}</h2>
                        <h2>Last Name : {{$value->last_name}}</h2>
                        <h2>Date of Birth : {{$value->date_of_birth}}</h2>
                        <h2>Place of Birth : {{$value->place_of_birth}}</h2>
                    </div>
                    <div class="card-footer">
                        <a href="#" class="btn btn-primary float-left">Followed</a>
                    </div>
                </div>
                @endif
                    
            @empty
                <tr colspan="3">
                    <td>You haven't follow anyone</td><br><br>
                    <td><a href="/profile/create" class="btn btn-primary">Create Profile</a></td>
                </tr>  
            @endforelse 
@endsection