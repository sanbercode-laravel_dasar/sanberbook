@extends('layouts.master')
@section('title')
    Create Profile
@endsection
@section('content')
<div class="card card-primary m-2">
    <div class="card-header">
      <h3 class="card-title">Profile Form</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
<form role="form" action="/profile/{{Auth::id()}}" method="POST">
    @csrf
    @method('PUT')
      <div class="card-body">
          <div class="form-group">
              <label for="first_name">First Name</label>
              <input type="text" class="form-control" id="first_name" name="first_name" value="{{$profile->first_name}}" placeholder="first name">
            </div>
          <div class="form-group">
              <label for="last_name">Last Name</label>
              <input type="text" class="form-control" id="last_name" name="last_name" value="{{$profile->last_name}}" placeholder="last name">
            </div>
          <div class="form-group">
            <label for="date_of_birth">Date of Birth</label>
            <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" value="{{$profile->date_of_birth}}" placeholder="date of birth">
            </div>
          <div class="form-group">
              <label for="place_of_birth">Place of Birth</label>
              <input type="text" class="form-control" id="place_of_birth" name="place_of_birth" value="{{$profile->place_of_birth}}" placeholder="place of birth">
            </div>
          
            
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Save Changes</button>
      </div>
    </form>
  </div>
@endsection