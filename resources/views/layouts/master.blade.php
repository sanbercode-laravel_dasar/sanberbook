<!doctype html>
<html lang="en">
  <head>
  	<title>SanberMedia | @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="{{asset('template/css/style.css')}}">
    @stack('script-head')
    </head>
  <body>	
	<div class="wrapper d-flex align-items-stretch">
        <nav id="sidebar">
            <a href="#" class="d-flex justify-content-center pt-5">{{Auth::user()->profile->first_name ?? 'first name'}} </a>
            <div class="p-4 pt-5">
                <ul class="list-unstyled components mb-5">
                    <li>
                        <a href="/post">Home</a>
                    </li>
                    <li>
                      <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Profile</a>
                      <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                          <a href="/profile/{{Auth::id()}}">My Profile</a>
                        </li>
                        <li>
                          <a class="list-unstyled components mb-5" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                          </a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                          </form>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="/profile">Friends List</a>
                  </li>
                </ul>
            </div>
        </nav>
           
        <div class="footer fixed-bottom bg-secondary text-center">
            <p class="p-1"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib.com</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
        </div>
            
        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5 mb-5">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <div class="container-fluid">

            <button type="button" id="sidebarCollapse" class="btn btn-primary">
              <i class="fa fa-bars"></i>
              <span class="sr-only">Toggle Menu</span>
            </button>

            <h1 class="m-auto text-center text-light">SanberMedia</h1>
          </div>
          <a href="/post/create" class="btn btn-primary float-right">Create post</a>
        </nav>
      @yield('content')
	    </div>
  </div>

    <script src="{{asset('template/js/jquery.min.js')}}"></script>
    <script src="{{asset('template/js/popper.js')}}"></script>
    <script src="{{asset('template/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('template/js/main.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    @stack('scripts')
  </body>
</html>