<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $guarded = [];
    // protected $fillable = ['first_name', 'last_name', 'date_of_birth', 'place_of_birth'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function post()
    {
        return $this->hasMany('App\Post');
    }

    public function comment()
    {
        return $this->hasMany('App\Comment');
    }

    public function images(){
        return $this->morphOne(Image::class, 'imageable');
    }

    public function reacts(){
        return $this->morphMany(Reaction::class, 'reactable');
    }
}