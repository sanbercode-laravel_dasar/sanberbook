<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reaction extends Model
{
    protected $guarded = [];
    
    public function reactable(){
        return $this->morphTo();
    }
}
