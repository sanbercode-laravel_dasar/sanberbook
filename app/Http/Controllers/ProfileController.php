<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        
        $profile = Profile::All();
        return view('profile.index', compact('profile'));
    }

    public function create()
    {
        return view('profile.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required|max:50',
            'last_name' => 'max:50',
            'date_of_birth' => 'required|date',
            'place_of_birth' => 'required|max:20',
        ]);
        
        $profile = $request->all();
        $profile['id'] = Auth::id();
        $profile['user_id'] = Auth::id();
        Profile::create($profile);
        return redirect('/profile');
    }
    
    public function show($id)
    {
        $profile = Profile::find($id);
        return view('profile.show', compact('profile'));
    }

    public function edit($id)
    {
        $profile = Profile::find($id);
        return view('profile.edit', compact('profile'));
    }

    public function update($id, Request $request)
    {
        // $request->validate([
        //     'first_name' => 'required|max:50',
        //     'last_name' => 'max:50',
        //     'date_of_birth' => 'required|date',
        //     'place_of_birth' => 'required',
        // ]);

        $update = Profile::where('id', $id)
        ->update([
            'first_name' => $request["first_name"],
            'last_name' => $request["last_name"],
            'date_of_birth' => $request["date_of_birth"],
            'place_of_birth' => $request["place_of_birth"]
        ]);
        return redirect('/profile/{{Auth::id()}}');
    }

    public function destroy($id)
    {
        profile::destroy($id);
        return redirect('/profile');
    }
}
