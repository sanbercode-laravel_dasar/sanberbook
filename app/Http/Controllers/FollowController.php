<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class FollowController extends Controller
{
    public function store(Request $request)
    {        
        $profile['id'] = Auth::id();
        $profile['user_id'] = Auth::id();
        Profile::create($profile);
        return redirect('/profile');
    }
}
