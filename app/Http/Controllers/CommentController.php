<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use App\Profile;
use Auth;


class CommentController extends Controller
{
    public function index() {
        $comment = Comment::All();
        $profiles = Profile::All();
        return view('comment.index', compact('comment','profiles'));
    }

    public function create()
    {
        return view('comment.create');
    }

    public function store(Request $request,$post_id)
    {
        $request->validate([
            'body' => 'required',
        ]);

        $post = Post::find($post_id);
        $comment = new Comment();
        $comment->body = $request->body;
        $comment->post()->associate($post);
        $comment->save();

        // return redirect()->route('post.show', [$post->slug]);
        
        // $comment = $request->all();
        // $comment['post_id'] = $post_id;
        // $comment['profile_id'] = Auth::id();
        // Comment::create($comment);
        return redirect('/post/{{$post->id}}');
    }
    
    // Sepertinya ini tidak dipakai
    public function show($id)
    {
        $comment = Comment::find($id);
        return view('comment.show', compact('comment'));
    }

    public function edit($id)
    {
        $comment = Comment::find($id);
        return view('comment.edit', compact('comment'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'body' => 'required',
        ]);

        Comment::where('id', $id)
        ->update([
            'body' => $request["body"]
        ]);
        return redirect('/post');
    }

    public function destroy($id)
    {
        // $query = DB::table('comment')->where('id', $id)->delete();
        Comment::destroy($id);
        return redirect('/post');
    }
}
