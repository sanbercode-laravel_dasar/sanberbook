<?php

namespace App\Http\Controllers;
use \App\Post;
use \App\Profile;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function index () {
        $posts = Post::All();
        $profiles = Profile::All();
        return view('post.index', compact('posts','profiles'));
    }
}
