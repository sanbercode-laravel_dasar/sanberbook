<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Profile;
use \App\Image;
use Auth;
use File;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        $posts = Post::All();
        $profiles = Profile::All();
        return view('post.index', compact('posts','profiles'));
    }

    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'caption' => 'required',
        ]);
        
        $post = $request->except('images');
        $post['profile_id'] = Auth::id();
        $post = Post::create($post);

        // Imageable
        $images = explode(",", $request->get('images'));
        foreach($images as $image){
            $query = [
                'imageable_id' =>$post->id,
                'imageable_type' => Image::class,
                'url' => $image
            ];
            Image::create($query);
            // Save images
            // $file = $request->file('images');
            // $extension = $file->getClientOriginalExtension();
            // Storage::disk('public')->put($file->getFilename().'.'.$extension,  File::get($file));
        }
        return redirect('/post');
    }
    
    public function show($id)
    {
        $post = Post::find($id);
        return view('post.show', compact('post'));
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'caption' => 'required',
        ]);

        Post::where('id', $id)
        ->update([
            'title' => $request["title"],
            'body' => $request["body"],
            'caption' => $request["caption"]
        ]);
        return redirect('/post');
    }

    public function destroy($id)
    {
        // $query = DB::table('post')->where('id', $id)->delete();
        Post::destroy($id);
        return redirect('/post');
    }
}
