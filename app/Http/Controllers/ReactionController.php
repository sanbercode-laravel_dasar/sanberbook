<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReactionController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'reaction' => 'boolean',
        ]);
        
        $profile = $request->all();
        $profile['id'] = Auth::id();
        $profile['user_id'] = Auth::id();
        Profile::create($profile);
        return redirect('/profile');
    }
}
