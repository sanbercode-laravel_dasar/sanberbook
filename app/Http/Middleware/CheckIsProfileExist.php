<?php

namespace App\Http\Middleware;

use Closure;
use \App\Profile;
use \App\User;
use Auth;

class CheckIsProfileExist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->profile->first_name){
            return $next($request);
        }
        return redirect(‘home’)->with(‘error’,"You must set profile.");
    }   
}