<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];

    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }
    
    public function images(){
        return $this->morphOne(Image::class, 'imageable');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }
}
